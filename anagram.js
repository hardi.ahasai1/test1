function anagram (a,b) {
    if (a.length !==  b.length){
        console.log('False');
        return
    }

    let stra = a.split('').sort().join('');
    let strb = b.split('').sort().join('');
    if (stra === strb){
        console.log("True");
    } else {
        console.log("False");
    }
}
anagram('anagram', 'nagaram');
anagram('aaz', 'zza');
